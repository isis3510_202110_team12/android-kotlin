package com.example.discountella.mapModule

import com.google.firebase.database.IgnoreExtraProperties
import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.GeoPoint

@IgnoreExtraProperties
data class Market(@DocumentId var id: String = "",
                  var name: String = "",
                  var image: String = "",
                  var location: GeoPoint? = null,
                  var chain: String = "")

