package com.example.discountella.mapModule.viewModel

import android.app.Activity
import android.app.Application
import android.content.Context
import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.discountella.mapModule.Repository.MapRepository
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng

class MapViewModel(context: Activity): ViewModel() {
    private var mapRepository = MapRepository(context)
    private var _lastLocation = MutableLiveData<Location>()
    var lastLocation : LiveData<Location>
        get() = _lastLocation
    init {
        lastLocation= _lastLocation
    }
    fun getLocation(onComplete:(Location)->Unit)
    {
        mapRepository.getLastLocation {
            if (it != null) {
                _lastLocation.postValue(it)
                onComplete(it)
            }
        }
    }
    class Factory(val context: Activity) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(MapViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return MapViewModel(context) as T
            }
            throw IllegalArgumentException("Unable to construct viewmodel")
        }
    }

}