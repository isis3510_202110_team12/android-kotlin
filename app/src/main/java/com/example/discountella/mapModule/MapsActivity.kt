package com.example.discountella.mapModule

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.discountella.R
import com.example.discountella.common.FireStoreSingletonDB
import com.example.discountella.common.Global
import com.example.discountella.mainModule.MainMenuActivity
import com.example.discountella.mapModule.viewModel.MapViewModel
import com.example.discountella.promotionModule.PromotionActivity
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

private var markerHolder= HashMap<Marker, Market>()
class MapsActivity : AppCompatActivity(), OnMapReadyCallback {


    private var marketList = mutableListOf<Market>()

    private lateinit var map: GoogleMap
    private val REQUEST_LOCATION_PERMISSION = 1
    private lateinit var viewModel:MapViewModel
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onStart() {
        //Global.Currentcontext = this
        super.onStart()

    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        // Obtain the FirebaseAnalytics instance.
        firebaseAnalytics = Firebase.analytics

        setContentView(R.layout.activity_maps)
        viewModel = ViewModelProvider(this, MapViewModel.Factory(this)).get(MapViewModel::class.java)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        val context = this

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        var  status = Global.isNetworkAvailable(this)
        if (status){
                Toast.makeText(
                    this,
                    "Conectado a internet ",
                    Toast.LENGTH_LONG
                ).show()
        }else{
            Toast.makeText(
                this,
                "Se esta usando informacion local, no hay coneccion a internet",
                Toast.LENGTH_LONG
            ).show()
        }

        Toast.makeText(this, "Seleccione un mercado para ver promociones disponibles", Toast.LENGTH_LONG).show()
        map.setOnMarkerClickListener{marker ->
            Global.currentMarket = markerHolder[marker]!!

            //Analitycs
            firebaseAnalytics.logEvent("View_Market"){
                param(FirebaseAnalytics.Param.ITEM_ID, Global.currentMarket.id)
                param(FirebaseAnalytics.Param.ITEM_NAME, Global.currentMarket.name)
            }

            val intent = Intent(this, PromotionActivity::class.java)
            startActivity(intent)
            true
        }
        //4.623754506541603, -74.14105070225332
        enableMyLocation()
        //adds market markers
        FireStoreSingletonDB.getInstance().queryMaps({
            var currentLL = LatLng(it.location!!.latitude, it.location!!.longitude);
            marketList.add(it)
            var nMarker = googleMap.addMarker(
                MarkerOptions()
                    .position(currentLL)
                    .title(it.name)
            )
            markerHolder[nMarker] = it
        },{
            Toast.makeText(this, "Lo sentimos, hay problemas de conexion a la base de datos, por favor intente mas tarde", Toast.LENGTH_LONG).show()
        })

        if(!Global.bestMarket.id.equals("noBest")){
            val builder = AlertDialog.Builder(this)
            builder.setMessage("Estas cerca a el mercado ${Global.bestMarket.name}, quieres ver las promociones disponibles?")
                .setCancelable(true)
                .setPositiveButton("Si"){ _, _ ->

                }.setNegativeButton("No"){ _, _ ->

                }.setNeutralButton("No quiero ver alertas"){ _, _ ->

                }
        }



    }


    private fun isPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    private fun enableMyLocation() {
        if (isPermissionGranted()) {
            if (ActivityCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            map.isMyLocationEnabled = true
        } else {
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf<String>(Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_LOCATION_PERMISSION
            )
        }

        viewModel.getLocation { val currentLatLong = LatLng(it.latitude, it.longitude)
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLong, 15f))
        Global.currentLatLong = currentLatLong}
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray) {
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults.contains(PackageManager.PERMISSION_GRANTED)) {
                enableMyLocation()
            }
        }
    }
}



