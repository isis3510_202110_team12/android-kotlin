package com.example.discountella.mapModule.Repository

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.location.Location
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng

class MapRepository (context: Activity){
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var appContext=context
    init
    {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(appContext)
    }
    @SuppressLint("MissingPermission")
    fun getLastLocation(onComplete:(Location)->Unit)
    {
        fusedLocationClient.lastLocation.addOnSuccessListener(appContext) {
            onComplete(it)
            }
        }
    }
