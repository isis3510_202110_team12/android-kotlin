package com.example.discountella.listModule

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isEmpty
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.discountella.R
import com.example.discountella.common.Global
import com.example.discountella.databinding.AddListBinding
import com.example.discountella.databinding.ItemAddListBinding
import com.example.discountella.productModule.Product
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase


class ActivityAddList : AppCompatActivity() {

    private val db = Firebase.firestore
    private lateinit var mBinding: AddListBinding
    private lateinit var mFirebaseAdapter: FirestoreRecyclerAdapter<Product, ProductsHolder>
    private lateinit var mLayoutManager: RecyclerView.LayoutManager

    private var  productsChecked : ArrayList<String> = ArrayList()


    private lateinit var firebaseAnalytics: FirebaseAnalytics


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = AddListBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        // Obtain the FirebaseAnalytics instance.
        firebaseAnalytics = Firebase.analytics
        val mContext = this

        var  status = Global.isNetworkAvailable(this)
        if (status){
            Toast.makeText(
                this,
                "Conectado a internet ",
                Toast.LENGTH_LONG
            ).show()
        }else{
            Toast.makeText(
                this,
                "Se esta usando informacion local, no hay conexion a internet",
                Toast.LENGTH_LONG
            ).show()
        }

        val query = db.collection("Product")

        val options = FirestoreRecyclerOptions.Builder<Product>().setQuery(query, Product::class.java)
            .setLifecycleOwner(this)
            .build()
        mFirebaseAdapter = object : FirestoreRecyclerAdapter<Product,ProductsHolder> (options){
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsHolder {
                val view = LayoutInflater.from(mContext)
                    .inflate(R.layout.item_add_list, parent, false)
                return ProductsHolder(view)
            }

            override fun onBindViewHolder(holder: ProductsHolder, position: Int, model: Product) {
                val product = getItem(position)

                with(holder) {
                    setListener(product)
                    binding.tvTitle.text = product.name
                    Glide.with(mContext)
                        .load(product.image)
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                        .centerCrop()
                        .into(binding.imgPhoto)

                    binding.checBox.setOnCheckedChangeListener { _, isChecked ->
                        if (isChecked) {
                            productsChecked.add(product.id)
                            Log.i("Chech products", "El producto fue agregado")
                        }else{
                            productsChecked.remove(product.id)
                            Log.i("Chech products", "El producto fue removido")
                        }
                    }
                }

            }

            override fun onError(error: FirebaseFirestoreException) {
                super.onError(error)
                Toast.makeText(mContext, error.message, Toast.LENGTH_SHORT).show()
            }
        }

        mLayoutManager = LinearLayoutManager(mContext)
        mBinding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = mLayoutManager
            adapter = mFirebaseAdapter

        }

        mBinding.buttonAdd.setOnClickListener {
            var user = Global.currentUser
            var title = mBinding.tfName.editText?.text.toString()

            if (!productsChecked.isEmpty() && title!="" ){

                //Analitycs
                firebaseAnalytics.logEvent("Action_AddList"){
                    param(FirebaseAnalytics.Param.ITEM_ID, user )
                }

                db.collection("List").document().set(
                    hashMapOf(
                        "title" to title,
                        "date" to "2020.05.02 14:05",
                        "idUser" to  user,
                        "products" to productsChecked
                    )
                )
                finish()
            }else if (productsChecked.isEmpty()){
                Toast.makeText(this, "Seleccione un producto", Toast.LENGTH_SHORT).show()
            }else {
                Toast.makeText(this, "Escriba un nombre a la lista", Toast.LENGTH_SHORT).show()
            }

        }

    }


    override fun onStart() {
        super.onStart()
        mFirebaseAdapter.startListening()
    }

    override fun onStop() {
        super.onStop()
        mFirebaseAdapter.stopListening()
    }

    inner class ProductsHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemAddListBinding.bind(view)

        fun setListener(product: Product) {

        }

    }

}