package com.example.discountella.listModule


import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.discountella.R
import com.example.discountella.common.FireStoreSingletonDB
import com.example.discountella.databinding.*
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.example.discountella.common.Global
import com.example.discountella.mapModule.MapsActivity
import com.example.discountella.productModule.ProductsActivity
import com.example.discountella.promotionModule.PromotionActivity
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent

class ActivityList : AppCompatActivity() {

    private val db = Firebase.firestore
    private lateinit var mBinding: ListRecyclerviewBinding
    private lateinit var mFireStoreAdapter: FirestoreRecyclerAdapter<ListProducts, ListViewHolder>
    private lateinit var mLayoutManager: RecyclerView.LayoutManager

    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onStart() {
        //Global.Currentcontext = this
        super.onStart()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ListRecyclerviewBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        var mContext = this
        // Obtain the FirebaseAnalytics instance.
        firebaseAnalytics = Firebase.analytics

        val query = FireStoreSingletonDB.getInstance().queryGetList()

        var  status = Global.isNetworkAvailable(this)
        if (status){
            Toast.makeText(
                this,
                "Conectado a internet ",
                Toast.LENGTH_LONG
            ).show()
        }else{
            Toast.makeText(
                this,
                "Se esta usando informacion local, no hay conexion a internet",
                Toast.LENGTH_LONG
            ).show()
        }


        val options = FirestoreRecyclerOptions.Builder<ListProducts>().setQuery(query, ListProducts::class.java)
                .setLifecycleOwner(this)
                .build()
        mFireStoreAdapter = object : FirestoreRecyclerAdapter<ListProducts, ListViewHolder>(options) {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
                val view = LayoutInflater.from(mContext).inflate(R.layout.row_list,parent,false)
                return ListViewHolder(view)
            }

            override fun onBindViewHolder(holder: ListViewHolder, position: Int, model: ListProducts) {
                val list = getItem(position)
                with(holder){
                    setListener(list)

                    binding.tvName.text = list.title
                    binding.tvDate.text = list.date
                    binding.tvNProducts.text = list.products!!.size.toString()
                    binding.editList.setOnClickListener { _: View ->

                        val builder = AlertDialog.Builder(this@ActivityList)
                        builder.setMessage("¿Quiere ver su lista o agregar nuevos productos a su lista?")
                                .setCancelable(true)
                                .setNeutralButton("Agregar") { _, _ ->
                                    Global.currentList = list
                                    Global.editarList = false
                                    val intent = Intent(this@ActivityList, ProductsListActivity::class.java)
                                    startActivity(intent)

                                }
                                .setPositiveButton("Ver") { dialog, _ ->
                                    if(list.products!!.size==0){
                                        Toast.makeText(this@ActivityList, "No tienes ningun producto en esta lista, por favor agregue productos antes de ver la lista", Toast.LENGTH_LONG).show()
                                        dialog.dismiss()
                                    }else{
                                        Global.currentList = list
                                        Global.editarList = true
                                        val intent = Intent(this@ActivityList, ProductsListActivity::class.java)
                                        startActivity(intent)
                                    }

                                }
                        val alert = builder.create()
                        alert.show()



                    }

                    binding.deleteList.setOnClickListener { _: View ->
                        val builder = AlertDialog.Builder(this@ActivityList)
                        builder.setMessage("¿Esta seguro que quiere eliminar esta lista?")
                                .setCancelable(false)
                                .setPositiveButton("Si") { _, _ ->
                                    val nombreList = list.title
                                    // Delete selected note from database
                                    db.collection("List").document(list.id)
                                            .delete()
                                            .addOnSuccessListener { Toast.makeText(this@ActivityList, "Lista: $nombreList a sido eliminada", Toast.LENGTH_SHORT).show() }
                                }
                                .setNegativeButton("No") { dialog, _ ->
                                    // Dismiss the dialog
                                    dialog.dismiss()
                                }
                        val alert = builder.create()
                        alert.show()
                    }

                    //Smart feature
                    binding.listBut.setOnClickListener {
                        var recFound= false
                        val papaAnca = arrayListOf("OtLBvzdNAv9cOYqN5In2","EctFFQM48Q9eLBo0j2AP")
                        val carbon = "cyDlU8GPJ7XaVNjwYe9g"
                        val oreo = "he4u1qsFjpSy3Ywc0xwE"
                        val leche = "5ajxrdX2UnY9W4ZNJnaE"
                        val cerveza = "7esTT2PnBPohjkgmnmZD"
                        val panal = "czZuosc6loJLCbhxzP8a"

                        //Analitycs
                        firebaseAnalytics.logEvent("Recomendacion"){
                            param(FirebaseAnalytics.Param.ITEM_ID,  list.id )
                            param(FirebaseAnalytics.Param.ITEM_NAME, list.idUser.toString() )
                        }

                        //asado1
                        if(list.products!!.containsAll(papaAnca) && !list.products!!.contains(carbon)){
                            recFound = true
                            val builder = AlertDialog.Builder(this@ActivityList)
                            builder.setMessage("En esta lista tiene Punta anca y papas, estas haciendo un asado? te recomendamos agregar carbon")
                                    .setCancelable(true)
                                    .setNeutralButton("ok"){ dialog, _ ->
                                        dialog.dismiss()
                                    }.create()
                            builder.show()
                        }
                        //asado 2
                        if(!list.products!!.containsAll(papaAnca) && list.products!!.contains(carbon) && !recFound){
                            recFound = true
                            val builder = AlertDialog.Builder(this@ActivityList)
                            builder.setMessage("En esta lista tiene carbon, estas haciendo un asado? te recomendamos agregar punta anca y papas")
                                    .setCancelable(true)
                                    .setNeutralButton("ok"){ dialog, _ ->
                                        dialog.dismiss()
                                    }.create()
                            builder.show()
                        }
                        //oreo 1
                        if(list.products!!.contains(oreo) && !list.products!!.contains(leche) && !recFound){
                            recFound = true
                            val builder = AlertDialog.Builder(this@ActivityList)
                            builder.setMessage("Esas oreos que tienes en tu lista irian bien con leche. Podrias agregar leche a tu lista")
                                    .setCancelable(true)
                                    .setNeutralButton("ok"){ dialog, _ ->
                                        dialog.dismiss()
                                    }.create()
                            builder.show()
                        }

                        //oreo 2
                        if(list.products!!.contains(leche) && !list.products!!.contains(oreo) && !recFound){
                            recFound = true
                            val builder = AlertDialog.Builder(this@ActivityList)
                            builder.setMessage("Tienes leche en tu lista, quiza podrias agregar unas oreos.")
                                    .setCancelable(true)
                                    .setNeutralButton("ok"){ dialog, _ ->
                                        dialog.dismiss()
                                    }.create()
                            builder.show()
                        }

                        //panal pola
                        if(list.products!!.contains(panal) && !list.products!!.contains(cerveza) && !recFound){
                            recFound = true
                            val builder = AlertDialog.Builder(this@ActivityList)
                            builder.setMessage("Eres un nuevo padre? quiza deberias comprar una Guinness para relajarte despues de acabar el dia")
                                    .setCancelable(true)
                                    .setNeutralButton("ok"){ dialog, _ ->
                                        dialog.dismiss()
                                    }.create()
                            builder.show()
                        }

                        //No recomendation found
                        if(!recFound){
                            Toast.makeText(this@ActivityList, "no tenemos recomendaciones para ${list.title}", Toast.LENGTH_SHORT).show()
                        }

                    }
                }
            }
            override fun onError(error: FirebaseFirestoreException) {
                super.onError(error)
                Toast.makeText(mContext, error.message, Toast.LENGTH_SHORT).show()
            }

        }

        mLayoutManager = LinearLayoutManager(mContext)
        mBinding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = mLayoutManager
            adapter = mFireStoreAdapter

        }

        mBinding.floatingButton.setOnClickListener{
            val intent = Intent( this , ActivityAddList::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }
    }


    inner class ListViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val binding = RowListBinding.bind(view)
        init{

        }
        fun setListener(List : ListProducts) {
        }
    }
}
