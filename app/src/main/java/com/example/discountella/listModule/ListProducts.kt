package com.example.discountella.listModule

import com.google.firebase.database.IgnoreExtraProperties
import com.google.firebase.firestore.DocumentId
import java.sql.Timestamp
import java.util.*
import kotlin.collections.ArrayList
@IgnoreExtraProperties
data class ListProducts (
        @DocumentId val id: String = "",
        var title : String? = null,
        var idUser : String? = null,
        var date: String? = null,
        var products : ArrayList<String>?=null)


