package com.example.discountella.listModule

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.discountella.R
import com.example.discountella.databinding.RowListBinding
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions

class ListAdapter(options: FirestoreRecyclerOptions<ListProducts>)
    : FirestoreRecyclerAdapter<ListProducts, ListAdapter.ListHolder>(options) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListHolder {
        return ListHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_list,parent,false))
    }

    override fun onBindViewHolder(holder: ListHolder, position: Int, model: ListProducts) {

        holder.name.text = model.idUser
        holder.date.text = model.date
        holder.nProd.text = model.products!!.size.toString()

    }

    inner class ListHolder(view: View):RecyclerView.ViewHolder(view) {

        val binding = RowListBinding.bind(view)
        var name = binding.tvName
        var date = binding.tvDate
        var nProd = binding.tvNProducts

    }
}