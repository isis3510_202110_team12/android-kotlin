package com.example.discountella.productModule

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.discountella.common.Global
import com.example.discountella.databinding.FragmentSetingsBinding
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.coroutines.flow.callbackFlow
import java.lang.Math.log
import java.lang.Math.random

class AddPrdActivity : AppCompatActivity() {
    private val RC_GALLERY = 18
    private val pathImg = "Images"

    private lateinit var mBinding : FragmentSetingsBinding
    private lateinit var mStoreReference : StorageReference

    private val db = FirebaseFirestore.getInstance()

    private var mPhotoSelectedUri: Uri? = null

    private lateinit var mcontext:Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = FragmentSetingsBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        mBinding.btnPost.setOnClickListener {
            if(mBinding.tfName.editText?.text.isNullOrEmpty()
                || mBinding.tfAmount.editText?.text.isNullOrEmpty()
                || mBinding.tfUnit.editText?.text.isNullOrEmpty()
                || mBinding.tfBrand.editText?.text.isNullOrEmpty()
            ){
                Toast.makeText(
                    this,
                    " Completa los campos necesarios",
                    Toast.LENGTH_LONG
                ).show()
            } else{
                postProduct()
            }
        }

        mBinding.btnSelect.setOnClickListener { openGallery() }

        mStoreReference = FirebaseStorage.getInstance().reference
        mcontext= this

    }

    private fun openGallery() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, RC_GALLERY)
    }

    private fun postProduct() {

        val storageRef = mStoreReference.child(pathImg)
            .child("product_img_${((random() * 10000000))}_${((random() * 10000000))}${((random() * 10000))}")

        val inputName = mBinding.tfName.editText?.text.toString()
        val inputAmount = mBinding.tfAmount.editText?.text.toString().toInt()
        val inputUnit = mBinding.tfUnit.editText?.text.toString()
        val inputBrand = mBinding.tfBrand.editText?.text.toString()

        db.collection("Product").whereEqualTo("brand",inputBrand)
            .whereEqualTo("name",inputName)
            .whereEqualTo("amount",inputAmount)
            .get()
            .addOnCompleteListener{ task ->
                if(task.result.isEmpty){
                    if (mPhotoSelectedUri != null ) {
                        mBinding.progressBar.visibility = View.VISIBLE
                        storageRef.putFile(mPhotoSelectedUri!!)
                            .addOnProgressListener {
                                var status = Global.isNetworkAvailable(this)
                                if (!status) {
                                    Toast.makeText(
                                        this,
                                        "El producto fue creado sin conexion, conectate para actualizar los cambios ",
                                        Toast.LENGTH_LONG
                                    ).show()
                                    mBinding.progressBar.visibility = View.INVISIBLE
                                    finish()
                                }
                                val progress = (100 * it.bytesTransferred / it.totalByteCount).toDouble()
                                mBinding.progressBar.progress = progress.toInt()
                                mBinding.tvMessage.text = "$progress%"
                            }
                            .addOnCompleteListener {
                                mBinding.progressBar.visibility = View.INVISIBLE
                            }
                            .addOnSuccessListener {
                                it.storage.downloadUrl.addOnSuccessListener {
                                    saveImg(
                                        it.toString(),
                                        inputName,
                                        inputAmount,
                                        inputUnit,
                                        inputBrand
                                    )
                                    Toast.makeText(
                                        this,
                                        "El producto fue creado ",
                                        Toast.LENGTH_LONG
                                    ).show()
                                    finish()
                                }
                            }
                            .addOnFailureListener {
                                //ERRORES
                                Snackbar.make(
                                    mBinding.root, "Error al crear producto",
                                    Snackbar.LENGTH_SHORT
                                ).show()
                            }
                    } else {
                        Toast.makeText(
                            this,
                            " Selecciona una foto",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }else{
                    Toast.makeText(
                        this,
                        "El producto ya existe ",
                        Toast.LENGTH_LONG
                    ).show()
                }
                Log.i("Exite",task.result.isEmpty.toString())
            }
    }

    private fun saveImg( url: String , name: String , amount: Int, unit: String, brand : String) {
        val cat: ArrayList<String> = arrayListOf()
        cat.add("trCdDPLnFhExFpU5Sp8D")

        db.collection("Product").document().set(
            hashMapOf(
                "amount" to amount,
                "name" to name,
                "brand" to brand,
                "categories" to cat,
                "unit" to unit,
                "image" to url
            )
        )

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK ){
            if (requestCode == RC_GALLERY)
                mPhotoSelectedUri = data?.data
            Glide.with(mcontext).load(mPhotoSelectedUri).into(mBinding.imgPhoto)
        }
    }
}