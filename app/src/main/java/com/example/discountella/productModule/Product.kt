package com.example.discountella.productModule

import com.google.firebase.database.IgnoreExtraProperties
import com.google.firebase.firestore.DocumentId

@IgnoreExtraProperties
data class Product(
                    @DocumentId val id: String = "",
                    var name:String = "",
                    var amount:Int=1,
                    var unit:String = "",
                    var brand:String ="",
                    var image:String ="")

