package com.example.discountella.productModule

import android.content.Context
import android.os.Bundle
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.discountella.R
import com.example.discountella.common.FireStoreSingletonDB
import com.example.discountella.common.Global
import com.example.discountella.databinding.*
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.*
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase


class ProductsActivity : AppCompatActivity() {

    private val db = Firebase.firestore
    private lateinit var mBinding: ActivityProductsBinding
    private lateinit var mFirebaseAdapter: FirestoreRecyclerAdapter<Product, ProductsHolder>
    private lateinit var mLayoutManager: RecyclerView.LayoutManager



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityProductsBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        var mContext = this

        var query = FireStoreSingletonDB.getInstance().queryGetProducts()

        var  status = Global.isNetworkAvailable(this)
        if (status){
            Toast.makeText(
                this,
                "Conectado a internet ",
                Toast.LENGTH_LONG
            ).show()
        }else{
            Toast.makeText(
                this,
                "Se esta usando informacion local, no hay coneccion a internet",
                Toast.LENGTH_LONG
            ).show()
        }

        val options = FirestoreRecyclerOptions.Builder<Product>().setQuery(query, Product::class.java)
                .setLifecycleOwner(this)
                .build()
        mFirebaseAdapter = object : FirestoreRecyclerAdapter<Product, ProductsHolder>(options) {

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsHolder {

                val view = LayoutInflater.from(mContext)
                        .inflate(R.layout.item_list, parent, false)
                return ProductsHolder(view)
            }

            override fun onBindViewHolder(holder: ProductsHolder, position: Int, model: Product) {

                val product = getItem(position)

                with(holder) {
                    setListener(product)

                    binding.tvTitle.text = product.name
                    Glide.with(mContext)
                            .load(product.image)
                            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                            .centerCrop()
                            .into(binding.imgPhoto)

                    if(Global.currentList.id.equals("null")){
                        //Vienes desde el home menu
                        binding.btnView.visibility = View.INVISIBLE
                    }else{
                        if(!Global.editarList) {
                            //Aca esta en modo agregar a la lista
                            binding.btnView.setOnClickListener {
                                db.collection("List").document(Global.currentList.id).update("products", FieldValue.arrayUnion(product.id)).addOnSuccessListener {
                                    Toast.makeText(this@ProductsActivity, "${product.name} agregado a la lista ${Global.currentList.title}", Toast.LENGTH_SHORT).show()
                                }.addOnFailureListener { _ ->
                                    Toast.makeText(this@ProductsActivity, "Error de conexion a base de datos, por favor intente mas tarde", Toast.LENGTH_LONG).show()

                                }
                            }
                        }else{
                            //Aca esta en modo eliminar de la lista
                            binding.btnView.rotationX  = 0.5F
                            binding.btnView.invalidate()
                            binding.btnView.setOnClickListener {
                                db.collection("List").document(Global.currentList.id).update("products", FieldValue.arrayRemove(product.id)).addOnSuccessListener {

                                    Toast.makeText(this@ProductsActivity, "${product.name} fue eliminado de la lista ${Global.currentList.title}", Toast.LENGTH_SHORT).show()
                                    finish()
                                }.addOnFailureListener { _ ->
                                    Toast.makeText(this@ProductsActivity, "Error de conexion a base de datos, por favor intente mas tarde", Toast.LENGTH_LONG).show()
                                }
                            }

                        }
                    }
                }
            }

            override fun onError(error: FirebaseFirestoreException) {
                super.onError(error)
                Toast.makeText(mContext, error.message, Toast.LENGTH_SHORT).show()
            }

        }

        mLayoutManager = LinearLayoutManager(mContext)
        mBinding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = mLayoutManager
            adapter = mFirebaseAdapter

        }
    }

    override fun onCreateView(name: String, context: Context, attrs: AttributeSet): View? {
        return super.onCreateView(name, context, attrs)
    }

    override fun onStart() {
        //Global.Currentcontext = this
        super.onStart()
        mFirebaseAdapter.startListening()
    }

    override fun onStop() {
        super.onStop()
        mFirebaseAdapter.stopListening()
    }

    inner class ProductsHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemListBinding.bind(view)
        fun setListener(product: Product) {
        }
    }
}