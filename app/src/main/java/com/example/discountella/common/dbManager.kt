package com.example.discountella.common
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import java.util.*
class dbManager {


    public fun setDocument(where:String, data:HashMap<String, Any>, id:String ){
        val db = Firebase.firestore
        db.collection(where).document(id).set(data, SetOptions.merge())

    }
    public  fun getDocuments(collection:String, field:String, value:String): QuerySnapshot {
        val db = Firebase.firestore
        //val getter = db.collection(collection)
        //.whereEqualTo(field, value)
        //.get().await()

        return db.collection(collection)
            .whereEqualTo(field, value)
            .get().result!!
    }




    /*
    val db = Firebase.firestore
    val result = db.collection(collection)
        .whereEqualTo(field, value)
        .get().result
    return result!!

     */

    fun genUser(id:String, birthdate:Long, email:String, gender:String, lastName:String, name:String){
        val user = hashMapOf(
            "name" to name,
            "lastName" to lastName,
            "birthdate" to birthdate,
            "email" to email,
            "gender" to gender
        )
        setDocument("Users", user, id)
    }

}
