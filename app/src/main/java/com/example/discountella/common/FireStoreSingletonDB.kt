package com.example.discountella.common

import android.widget.Toast
import com.example.discountella.mapModule.Market
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.firestore.FieldPath
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class FireStoreSingletonDB {
    companion object {
        private var instance: FireStoreSingletonDB? = null
        fun getInstance() =
            instance ?: synchronized(this) {
                instance ?: FireStoreSingletonDB().also {
                    instance = it
                }
            }
    }

    private val db = Firebase.firestore

    fun queryPromotion():Query
    {
        return db.collection("Promotion").whereEqualTo("chain", Global.currentMarket.chain)
    }

    fun queryGetProducts():Query
    {
        return if(!Global.editarList){
            db.collection("Product")
        }else{
            db.collection("Product").whereIn(FieldPath.documentId(), Global.currentList.products!!)
        }

    }

    fun queryMaps(onComplete:(Market)->Unit, onFailure:()->Unit)
    {
        db.collection("Market")
            .get().addOnSuccessListener { markets ->
                lateinit var nMarket: Market
                for(market in markets.documents){
                    nMarket = Market()
                    nMarket.id= market.id
                    nMarket.name =  market.get("name").toString()
                    nMarket.chain = market.get("chain").toString()
                    nMarket.location = market.getGeoPoint("location")
                    nMarket.image = market.get("image").toString()
                    onComplete(nMarket)
                }
            }.addOnFailureListener{ _ ->
                onFailure()
            }
    }

    fun queryGetList():Query
    {
        return db.collection("List").whereEqualTo("idUser",Global.currentUser)
    }

}
