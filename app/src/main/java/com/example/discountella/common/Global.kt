package com.example.discountella.common

import  android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import com.example.discountella.listModule.ListProducts
import com.example.discountella.mapModule.Market
import com.example.discountella.mapModule.viewModel.MapViewModel
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.functions.ktx.functions
import com.google.firebase.ktx.Firebase

class Global: Application()  {
    companion object {


        var currentLatLong = LatLng(0.000888,0.000888)
        var functions = Firebase.functions



        //lateinit var Currentcontext: Context
        var running: Boolean = true
        var editarList: Boolean = false
        var currentList:ListProducts= ListProducts("null", "null","null","null",null)
        var currentUser: String = ""
        lateinit var currentMarket: Market
        var bestMarket = Market("noBest")

        fun isNetworkAvailable(context: Context): Boolean {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val nw      = connectivityManager.activeNetwork ?: return false
                val actNw = connectivityManager.getNetworkCapabilities(nw) ?: return false
                return when {
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                    //for other device how are able to connect with Ethernet
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                    else -> false
                }
            } else {
                return connectivityManager.activeNetworkInfo?.isConnected ?: false
            }
        }
    }


}