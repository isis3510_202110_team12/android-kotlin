package com.example.discountella.promotionModule

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.discountella.R
import com.example.discountella.common.FireStoreSingletonDB
import com.example.discountella.common.Global
import com.example.discountella.databinding.ActivityPromotionBinding
import com.example.discountella.databinding.PromotionListBinding
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class PromotionActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivityPromotionBinding
    private lateinit var mFirebaseAdapter: FirestoreRecyclerAdapter<Promotion,PromotionHolder>
    private lateinit var mLayoutManager: RecyclerView.LayoutManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityPromotionBinding.inflate(layoutInflater)
        setContentView(mBinding.root)


        //Sets market data
        mBinding.nombreMercadoPr.text = Global.currentMarket.name

        Glide.with(this)
            .load(Global.currentMarket.image)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
            .into(mBinding.imgMercado)

        var mContext = this

        val query = FireStoreSingletonDB.getInstance().queryPromotion();

        val options = FirestoreRecyclerOptions.Builder<Promotion>().setQuery(query, Promotion::class.java)
            .setLifecycleOwner(this)
            .build()

        mFirebaseAdapter = object : FirestoreRecyclerAdapter<Promotion, PromotionHolder>(options){
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PromotionHolder {
                val view= LayoutInflater.from(mContext)
                    .inflate(R.layout.promotion_list,parent, false)
                return PromotionHolder(view)
            }

            override fun onBindViewHolder(holder: PromotionHolder, position: Int, model: Promotion) {
                val promotion = getItem(position)

                with(holder){
                    setListener(promotion)

                    binding.promoDesc.text = promotion.description
                    binding.precioOriginalPromo.text = promotion.OriginalPrice.toString()
                    binding.precioNuevoPromo.text = promotion.newPrice.toString()

                }
            }

            override fun onError(error: FirebaseFirestoreException) {
                super.onError(error)
            }
        }

        mLayoutManager = LinearLayoutManager(mContext)
        mBinding.promoRecycler.apply {
            setHasFixedSize(true)
            layoutManager = mLayoutManager
            adapter = mFirebaseAdapter

        }


    }


    override fun onCreateView(name: String, context: Context, attrs: AttributeSet): View? {
        return super.onCreateView(name, context, attrs)
    }

    override fun onStart() {
        //Global.Currentcontext = this
        super.onStart()
        mFirebaseAdapter.startListening()
    }

    override fun onStop() {
        super.onStop()
        mFirebaseAdapter.stopListening()
    }


    inner class PromotionHolder(view: View): RecyclerView.ViewHolder(view) {
        val binding = PromotionListBinding.bind(view)
        fun setListener(promotion: Promotion){

        }
    }
}