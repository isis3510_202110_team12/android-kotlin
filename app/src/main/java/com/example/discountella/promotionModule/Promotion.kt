package com.example.discountella.promotionModule

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class Promotion(var description: String = "",
                     var OriginalPrice: Double= 0.0,
                     var newPrice: Double= 0.0) {
}