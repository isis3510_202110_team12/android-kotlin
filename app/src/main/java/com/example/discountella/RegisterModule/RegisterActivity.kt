package com.example.discountella.RegisterModule

//import android.R
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.discountella.R
import com.example.discountella.common.dbManager
import com.example.discountella.mainModule.MainActivity
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import java.lang.Exception
import java.text.SimpleDateFormat
import com.example.discountella.databinding.ActivityRegisterBinding



private lateinit var firebaseAnalytics: FirebaseAnalytics
private lateinit var mBinding: ActivityRegisterBinding

class RegisterActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {

        // Obtain the FirebaseAnalytics instance.
        firebaseAnalytics = Firebase.analytics

// Initialize Firebase Auth
        var auth: FirebaseAuth = Firebase.auth


        super.onCreate(savedInstanceState)
        mBinding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        //gets values into spinner for gender and id type
        val dropdown = mBinding.idSelectReg
        val dropdown2 = mBinding.generoSelect
        val items = arrayOf("","Cedula Ciudanania", "Cedula Extranjeria", "Targeta Identidad", "Pasaporte")
        val itemsGen = arrayOf("","Hombre","Mujer","Otro")
        var adapter =
            ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, items)

        dropdown.adapter = adapter

        adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, itemsGen)
        dropdown2.adapter =adapter
        //goes back to login screen when pressing cancel
        mBinding.cancelReg.setOnClickListener { _ ->
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
        //Register checks if all fields are filled and registers new user in firebase if possible
        mBinding.regReg.setOnClickListener{
            var correctEntry = true
            //check name
            if(TextUtils.isEmpty(mBinding.emailLogin.text.toString().trim{it <= ' '})){
                Toast.makeText(this, "Por favor ingrese su nombre", Toast.LENGTH_SHORT).show()
                correctEntry=false
            }
            //checks last name
            if(TextUtils.isEmpty(mBinding.passLogin.text.toString().trim{it <= ' '})){
                Toast.makeText(this, "Por favor ingrese su apellido", Toast.LENGTH_SHORT).show()
                correctEntry=false
            }
            //checks last name
            if(TextUtils.isEmpty(mBinding.passLogin.text.toString().trim{it <= ' '})){
                Toast.makeText(this, "Por favor ingrese su apellido", Toast.LENGTH_SHORT).show()
                correctEntry=false
            }
            //Checks if birth date is valid
            val datePicker = mBinding.birthdateReg

            var year = datePicker.year
            var month = datePicker.month.toInt()
            var day = datePicker.dayOfMonth

            val current = System.currentTimeMillis()
            val date = SimpleDateFormat("dd-MM-yyyy").parse("$day-$month-$year")

            if(date.time>=current){
                Toast.makeText(this, "Fecha invalida", Toast.LENGTH_SHORT).show()
                correctEntry=false
            }

            //Check tipoDoc
            if(mBinding.idSelectReg.selectedItem.toString().equals("")){
                Toast.makeText(this, "Seleccione tipo de documento", Toast.LENGTH_SHORT).show()
                correctEntry=false
            }
            if(TextUtils.isEmpty(mBinding.idReg.text.toString().trim{it <= ' '})){
                Toast.makeText(this, "Por favor ingrese numero documento", Toast.LENGTH_SHORT).show()
                correctEntry=false
            }


            if(mBinding.generoSelect.selectedItem.toString().equals("")){
                Toast.makeText(this, "Seleccione un genero", Toast.LENGTH_SHORT).show()
                correctEntry=false
            }
            //checks email
            if(TextUtils.isEmpty(mBinding.emailReg.text.toString().trim{it <= ' '})){
                Toast.makeText(this, "Por favor ingrese un correo", Toast.LENGTH_SHORT).show()
                correctEntry=false
            }
            if(!android.util.Patterns.EMAIL_ADDRESS.matcher(mBinding.emailReg.text.toString()).matches()){
                Toast.makeText(this, "Por favor ingrese un correo valido", Toast.LENGTH_SHORT).show()
                correctEntry=false
            }
            //password check
            if(TextUtils.isEmpty(mBinding.regPassword.text.toString().trim{it <= ' '})){
                Toast.makeText(this, "Por favor una contraseña", Toast.LENGTH_SHORT).show()
                correctEntry=false
            }
            // olongerr equal 6
            if(!(mBinding.regPassword.text.toString().length>=6)){
                Toast.makeText(this, "La contraseña debe ser mayor a 6 digitos", Toast.LENGTH_SHORT).show()
                correctEntry=false
            }
            if(TextUtils.isEmpty(mBinding.regPassword2.text.toString().trim{it <= ' '})){
                Toast.makeText(this, "por favor vuelva a ingresar contraseña", Toast.LENGTH_SHORT).show()
                correctEntry=false
            }

            var pass1= mBinding.regPassword.text.toString()
            var pass2= mBinding.regPassword2.text.toString()
            if(!pass1.equals(pass2)){
                Toast.makeText(this, "las contraseñas no son iguales", Toast.LENGTH_SHORT).show()
                correctEntry=false
            }

            if(correctEntry){

                var name= mBinding.emailLogin.text.toString()
                var lastname= mBinding.passLogin.text.toString()
                val idRegText= mBinding.idReg.text.toString()
                var userId= ""
                when(mBinding.idSelectReg.selectedItem.toString()){
                    items[1] -> userId= "CC$idRegText"
                    items[2] -> userId= "CE$idRegText"
                    items[3] -> userId= "TI$idRegText"
                    items[4] -> userId= "PA$idRegText"
                }
                //Toast.makeText(this, userId, Toast.LENGTH_SHORT).show()

                var email = mBinding.emailReg.text.toString()
                var gender = mBinding.generoSelect.selectedItem.toString()


                val dbm = dbManager()
                val params = Bundle()
                params.putString("method", "Register app")
                firebaseAnalytics.logEvent("Register",params)
                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP,params)

                auth.createUserWithEmailAndPassword(email,pass1).addOnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        try {
                            throw task.exception!!
                        } catch (e: FirebaseNetworkException) {
                            Log.i("Exeption", "${e.message.toString()}")
                            Toast.makeText(this, "Problemas en la red intente mas tarde ", Toast.LENGTH_SHORT).show()
                        } catch (e: Exception) {
                            Log.i("Exeption", "${e.toString()}")
                            Toast.makeText(this, "Ocurrio un problema, vuelva intentar", Toast.LENGTH_SHORT).show()
                        }
                    }else {
                        Toast.makeText(this, "Registrando", Toast.LENGTH_SHORT).show()
                        dbm.genUser(userId, date.time, email, gender, lastname, name)
                        val intent =Intent(this, MainActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        intent.putExtra("email", email)
                        startActivity(intent)
                        finish()
                    }
                }
            }

        }
    }
}