package com.example.discountella.mainModule

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.discountella.R
import com.example.discountella.RegisterModule.RegisterActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.example.discountella.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {


    private  var auth = Firebase.auth
    private lateinit var mAuthListener: FirebaseAuth.AuthStateListener
    private var mFirebaseAuth: FirebaseAuth? = null
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    private lateinit var mBinding: ActivityMainBinding;


    override fun onCreate(savedInstanceState: Bundle?) {

        // Obtain the FirebaseAnalytics instance.

        firebaseAnalytics = Firebase.analytics

        super.onCreate(savedInstanceState)

        mBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        //setSupportActionBar(findViewById(R.id.toolbar))
        //setSupportActionBar(mBinding.toolbar)



        //Starts register activity


        mBinding.innerContent.RegLink.setOnClickListener { _ ->
            startActivity(Intent(this, RegisterActivity::class.java))
        }

        mBinding.innerContent.loginButton.setOnClickListener{ _ ->
            var email =  mBinding.innerContent.emailLogin.text.toString()
            var pass =  mBinding.innerContent.passLogin.text.toString()
            if (email.isEmpty() || pass.isEmpty()){
                Toast.makeText(this, "Completa los campos", Toast.LENGTH_SHORT).show()
            }else {
                login(email,pass)
            }
        }
    }

    private fun login(email:String, pass:String){
        mFirebaseAuth = FirebaseAuth.getInstance()

        auth.signInWithEmailAndPassword(email, pass).addOnCompleteListener { task ->
            if (!task.isSuccessful){
                try {
                    throw task.exception!!
                }catch (e:FirebaseAuthInvalidCredentialsException ){
                    Log.i("Exeption","${e.message.toString()}")
                    Toast.makeText(this,"Email o Contraseña incorrecta", Toast.LENGTH_SHORT).show()
                }catch (e:FirebaseNetworkException ){
                    Log.i("Exeption","${e.message.toString()}")
                    Toast.makeText(this, "No se tiene conexion a internet, revisa tu conexion ", Toast.LENGTH_SHORT).show()
                }
            }else{
                Toast.makeText(this, "Ingreso Exitoso", Toast.LENGTH_SHORT).show()
                val firebaseUser: FirebaseUser = task.result!!.user!!
                val params = Bundle()
                params.putString("Method", "Ingreso App")
                firebaseAnalytics.logEvent("Sign_In",params)
                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN,params)

                val intent = Intent(this, MainMenuActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                intent.putExtra("userID", firebaseUser.uid)
                intent.putExtra("email", firebaseUser.email)

                val storage = this.getSharedPreferences("Credenciales", Context.MODE_PRIVATE)
                storage.edit().putString("userName",email).apply()
                storage.edit().putString("userPass",pass).apply()

                startActivity(intent)
                mAuthListener = FirebaseAuth.AuthStateListener {
                    val user = auth.currentUser
                }
                finish()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.bottom_nav_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()
        mFirebaseAuth?.addAuthStateListener(mAuthListener)
    }

    override fun onPause() {
        super.onPause()
        mFirebaseAuth?.removeAuthStateListener(mAuthListener)
    }

}