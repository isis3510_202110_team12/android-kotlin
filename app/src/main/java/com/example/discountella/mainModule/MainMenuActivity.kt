package com.example.discountella.mainModule

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.discountella.R
import com.example.discountella.common.Global
import com.example.discountella.common.Global.Companion.functions
import com.example.discountella.databinding.ActivityHomeBinding
import com.example.discountella.listModule.ProductsListActivity
import com.example.discountella.mapModule.Market
import com.example.discountella.promotionModule.PromotionActivity
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.functions.FirebaseFunctionsException
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.*
import kotlinx.coroutines.tasks.await
import java.util.*
import kotlin.concurrent.schedule
import kotlin.math.cos
import kotlin.math.sqrt
import kotlinx.coroutines.tasks.await
import org.json.JSONObject
import kotlin.collections.HashMap


class MainMenuActivity: AppCompatActivity() {

    private val db = Firebase.firestore
    private lateinit var mBinding: ActivityHomeBinding
    private lateinit var mActivieFragment: Fragment
    private lateinit var mFragmentManager: FragmentManager
    private lateinit var mfirebaseAnalytics: FirebaseAnalytics

    override fun onStart() {
        //Global.Currentcontext = this

        val scope = CoroutineScope(Job() + Dispatchers.IO)

        scope.launch {
            locationAlerter()
        }
        super.onStart()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mfirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        if(Global.currentUser == "") {

            val userEmail = intent.getStringExtra("email")
            val query = db.collection("Users").whereEqualTo("email", userEmail)
            query.get().addOnSuccessListener { user ->
                Log.d(
                    "QUERY",
                    "query successful"
                ); Global.currentUser = user.documents[0].id
            }
        }

        mBinding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        setupBottomNav()
        mBinding.bottomNav.selectedItemId = R.id.action_home
    }

    private fun setupBottomNav() {
        mFragmentManager = supportFragmentManager

        val homeFragment = HomeFragment()
        val porfileFragment = PorfileFragment()
        val setingsFragment = SetingsFragment()

        mActivieFragment = homeFragment

        mFragmentManager.beginTransaction()
            .add(R.id.hostFragment, porfileFragment, PorfileFragment::class.java.name)
            .hide(porfileFragment).commit()

        mFragmentManager.beginTransaction()
            .add(R.id.hostFragment, setingsFragment, SetingsFragment::class.java.name)
            .hide(setingsFragment).commit()

        mFragmentManager.beginTransaction()
            .add(R.id.hostFragment, homeFragment, HomeFragment::class.java.name).commit()

        mBinding.bottomNav.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.action_home -> {
                    mFragmentManager.beginTransaction().hide(mActivieFragment).show(homeFragment)
                        .commit()
                    mActivieFragment = homeFragment

                    val params = Bundle()
                    params.putString("ACTION", "HOME" )
                    mfirebaseAnalytics.logEvent("Action_Nav", params)

                    true
                }
                R.id.action_porfile -> {
                    mFragmentManager.beginTransaction().hide(mActivieFragment).show(porfileFragment)
                        .commit()
                    mActivieFragment = porfileFragment

                    val params = Bundle()
                    params.putString("ACTION", "Profile" )
                    mfirebaseAnalytics.logEvent("Action_Nav", params)
                    true
                }
                R.id.action_settings -> {
                    mFragmentManager.beginTransaction().hide(mActivieFragment).show(setingsFragment)
                        .commit()
                    mActivieFragment = setingsFragment
                    val params = Bundle()
                    params.putString("ACTION", "Setings ")
                    mfirebaseAnalytics.logEvent("Action_Nav", params)
                    true
                }
                else -> false
            }
        }
    }

    suspend fun locationAlerter() {
        Log.d("ENTRE", "Entre ${Global.currentLatLong}")
        withContext(Dispatchers.Default) {
                //cada dos minutos revisa
                Timer("ThreadTest", false).schedule(30000) {
                    if (Global.running && Global.currentLatLong.latitude != 0.000888 && Global.currentLatLong.longitude != 0.000888) {

                         functions
                            .getHttpsCallable("getNearestMarket")
                            .call(hashMapOf("latitude" to Global.currentLatLong.latitude, "longitude" to Global.currentLatLong.longitude))
                            .addOnCompleteListener(OnCompleteListener { task ->
                            if (!task.isSuccessful) {
                                val e = task.exception
                                Log.d("task",e.toString())
                                if (e is FirebaseFunctionsException) {
                                    val code = e.code
                                    val details = e.details
                                }

                                // ...
                            }else
                            {
                                Log.d("TASK",task.result.toString())
                                val result:HashMap<String,Any> = task.result?.data as HashMap<String, Any>
                                Log.d("result",result.toString())
                                val p = result.get("position") as HashMap<String,Any>
                                val gp = p.get("geopoint") as HashMap<String,Any>
                                val bestMarket=Market(id= result.get("id") as String,name=result.get("name") as String,image = result.get("image") as String
                                    ,chain = result.get("chain") as String,location = GeoPoint(gp.get("Latitude") as Double,gp.get("Longitude") as Double)
                                )
                                Global.bestMarket =bestMarket

                                val builder = AlertDialog.Builder(this@MainMenuActivity)
                                builder.setMessage("Estas cerca a el mercado ${Global.bestMarket.name}, quieres ver las promociones disponibles?")
                                    .setCancelable(true)
                                    .setPositiveButton("Si"){ _, _ ->
                                        Global.currentMarket = Global.bestMarket

                                        //Analitycs
                                        mfirebaseAnalytics.logEvent("Use_ContAwa"){
                                            param(FirebaseAnalytics.Param.ITEM_ID, Global.currentUser )
                                        }

                                        val intent = Intent(this@MainMenuActivity, PromotionActivity::class.java)
                                        startActivity(intent)

                                    }.setNegativeButton("No"){ dialog, _ ->
                                        dialog.dismiss()

                                    }.setNeutralButton("No quiero ver alertas"){ _, _ ->
                                        Global.running = false

                                    }
                                val alert = builder.create()
                                alert.show()
                            }

                            // ...
                        })

                        }
                    }
                }
        }


    }



