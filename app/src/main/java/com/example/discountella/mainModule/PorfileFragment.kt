package com.example.discountella.mainModule

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.discountella.R
import com.example.discountella.databinding.FragmentPorfileBinding
import com.example.discountella.databinding.ItemListBinding
import com.example.discountella.listModule.ListProducts
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class PorfileFragment : Fragment() {

    private lateinit var mBinding: FragmentPorfileBinding
    private lateinit var mLayoutManager: RecyclerView.LayoutManager


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_porfile, container, false)
    }



}