package com.example.discountella.mainModule

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.discountella.common.Global
import com.example.discountella.productModule.ProductsActivity
import com.example.discountella.databinding.PrincipalMenuBinding
import com.example.discountella.listModule.ActivityList
import com.example.discountella.listModule.ListProducts
import com.example.discountella.mapModule.MapsActivity
import com.example.discountella.productModule.AddPrdActivity
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase

class HomeFragment : Fragment() {

    private lateinit var  mBinding: PrincipalMenuBinding

    private lateinit var firebaseAnalytics: FirebaseAnalytics
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = PrincipalMenuBinding.inflate(inflater,container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Obtain the FirebaseAnalytics instance.
        firebaseAnalytics = Firebase.analytics

        mBinding.btnSuperCerca.setOnClickListener{
            //Analitycs
            firebaseAnalytics.logEvent("Action_NearbyMkts"){
                param(FirebaseAnalytics.Param.ITEM_NAME, "Ver Mercados" )
            }
            val intent = Intent( activity , MapsActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }

        mBinding.btnAddPrd.setOnClickListener {
            val intent = Intent( activity , AddPrdActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }

        mBinding.btnMakeList.setOnClickListener{
            val intent = Intent( activity , ActivityList::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }

        mBinding.btnseePrd.setOnClickListener {
            Global.currentList = ListProducts("null", "null","null","null",null)
            Global.editarList = false
            //Analitycs
            firebaseAnalytics.logEvent("Action_SeeProducts"){
                param(FirebaseAnalytics.Param.ITEM_NAME, "Ver Productos" )
            }
            val intent = Intent( activity , ProductsActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }
    }
}