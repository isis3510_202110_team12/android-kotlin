package com.example.discountella.mainModule

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class Users(var email:String = "",
                var birthdate:Int = 0,
                var gender:String = "",
                var brand:String ="",
                var image:String ="")

